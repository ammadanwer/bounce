#ifndef _CONMANIP_H_
#define _CONMANIP_H_

#pragma once
#include <iostream>
#include <windows.h>


inline std::ostream& BLUE(std::ostream &s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
    SetConsoleTextAttribute(hStdout, FOREGROUND_BLUE
              |FOREGROUND_GREEN|FOREGROUND_INTENSITY);
    return s;
}

inline std::ostream& RED(std::ostream &s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
    SetConsoleTextAttribute(hStdout, 
                FOREGROUND_RED|FOREGROUND_INTENSITY);
    return s;
}

inline std::ostream& GREEN(std::ostream &s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
    SetConsoleTextAttribute(hStdout, 
              FOREGROUND_GREEN|FOREGROUND_INTENSITY);
    return s;
}

inline std::ostream& YELLOW(std::ostream &s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
    SetConsoleTextAttribute(hStdout, 
         FOREGROUND_GREEN|FOREGROUND_RED|FOREGROUND_INTENSITY);
    return s;
}

inline std::ostream& WHITE(std::ostream &s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
    SetConsoleTextAttribute(hStdout, 
       FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);
    return s;
}

struct color {
    color(WORD attribute):m_color(attribute){};
    WORD m_color;
};

template <class _Elem, class _Traits>
std::basic_ostream<_Elem,_Traits>& 
      operator<<(std::basic_ostream<_Elem,_Traits>& i, color& c)
{
    HANDLE hStdout=GetStdHandle(STD_OUTPUT_HANDLE); 
    SetConsoleTextAttribute(hStdout,c.m_color);
    return i;
}

void gotorc(int r, int c){
	HANDLE console_handle;
	COORD cursor_coord;
	cursor_coord.X=c; 
	cursor_coord.Y=r;
	console_handle= GetStdHandle( STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(console_handle,cursor_coord);
}

void putChar(const char * ch, int col){
	switch(col){
		case 0:
			cout<<WHITE<<ch;
			break;
		case 1:
			cout<<RED<<ch;
			break;
		case 2:
			cout<<GREEN<<ch;
			break;
		case 3:
			cout<<BLUE<<ch;
			break;
		case 4:
			cout<<YELLOW<<ch;
			break;

		default:
			cout<<ch;
			break;
	};
	cout<<WHITE;
}

#endif